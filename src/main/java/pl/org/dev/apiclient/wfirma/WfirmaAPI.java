package pl.org.dev.apiclient.wfirma;

import org.scribe.builder.api.DefaultApi10a;
import org.scribe.model.Token;
import org.scribe.services.PlaintextSignatureService;
import org.scribe.services.SignatureService;

/**
 * Created with IntelliJ IDEA.
 * User: cyprian
 * Date: 30.10.13
 * Time: 21:47
 * To change this template use File | Settings | File Templates.
 */
public class WfirmaAPI extends DefaultApi10a
{
    private static final String AUTHORIZE_URL = "https://wfirma.pl/oauth/authorize?oauth_token=%s";
    private static final String REQUEST_TOKEN_RESOURCE = "wfirma.pl/oauth/requestToken?oauth_callback=https%3A%2F%2Ffirmacallback&scope=oauth-read";
    private static final String ACCESS_TOKEN_RESOURCE = "wfirma.pl/oauth/accessToken";

    @Override
    public String getAccessTokenEndpoint()
    {
        return "https://" + ACCESS_TOKEN_RESOURCE;
    }

    @Override
    public String getRequestTokenEndpoint()
    {
        return "https://" + REQUEST_TOKEN_RESOURCE;
    }

    @Override
    public String getAuthorizationUrl(Token requestToken)
    {
        return String.format(AUTHORIZE_URL, requestToken.getToken());
    }

    @Override
    public SignatureService getSignatureService()
    {
        return new PlaintextSignatureService();
    }
}
