package pl.org.dev.apiclient.wfirma;
/*
 * Licensed to HMail.pl under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  HMail.pl licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxrs.ext.form.Form;
import org.apache.cxf.rs.security.oauth.client.OAuthClientUtils;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Properties;

/**
 * @author Cyprian Śniegota
 */
public class WfirmaApiClient {

    public static final String BASE_ADDRESS = "https://api2.wfirma.pl/";
    private static final String SERVICE_API_PREFIX_URL = "https://api2.wfirma.pl/";

    private enum ERequestMethod {
        GET, POST
    }

    private enum EInvoiceDocType {
        ORG("org"), KOPIA("KOPIA"), ORG_KOP("org_kop"), D_ORG("d_org"), D_KOP("d_kop"),
        REGULAR("regular"), DOUBLE_REGULAR("double_regular"), DUPLICATE("duplicate"),
        ANG("ang"), ANGPOL("angpol");
        private String code;

        private EInvoiceDocType(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }
    }


    private String executeServiceOAuth(AuthCreditials authCreditials, ERequestMethod requestMethod, String serviceUrl, Properties queryProperties, String data, String servicePrefixUrl) throws WfirmaApiException {
        Response response = null;
        String returnString = "{}";
        try {
            WebClient client = WebClient.create(servicePrefixUrl);
            client.path(serviceUrl);

            StringBuilder requestUrlBuilder;
            requestUrlBuilder = new StringBuilder();
            requestUrlBuilder.append(servicePrefixUrl).append(serviceUrl);
            if (queryProperties != null) {
                Enumeration<?> enumeration;
                enumeration = queryProperties.propertyNames();
                while (enumeration.hasMoreElements()) {
                    String name = (String) enumeration.nextElement();
                    String value = StringUtils.trimToNull(queryProperties.getProperty(name));
                    if (value != null) {
                        client.query(name, value);
                    }
                }
            }
            OAuthClientUtils.Consumer consumer = new OAuthClientUtils.Consumer(authCreditials.getConsumerKey(), authCreditials.getConsumerSecret());

            String authorizationHeader = OAuthClientUtils.createAuthorizationHeader(
                    consumer, new OAuthClientUtils.Token(authCreditials.getAccessToken(), authCreditials.getSecretToken()),
                    requestMethod.name(), client.getCurrentURI().toString());

            client.header("Authorization", authorizationHeader)
                    .header("Accept", "application/json").header("Content-type", "application/json");
            if (requestMethod.equals(ERequestMethod.GET)) {
                response = client.get(Response.class);
            } else if (requestMethod.equals(ERequestMethod.POST)) {
                response = client.post(data, Response.class);
            }
        } catch (Exception e) {
            throw new WfirmaApiException("Error running service [" + serviceUrl + "]", e);
        }
        returnString = response != null ? response.readEntity(String.class) : "{}";
        return returnString;
    }

    private String executeService(AuthCreditials authCreditials, ERequestMethod requestMethod, String serviceUrl, Properties queryProperties, String data, String servicePrefixUrl) throws WfirmaApiException {
        Response response = null;
        String returnString = "{}";
        try {
            WebClient client = WebClient.create(servicePrefixUrl, authCreditials.getUsername(), authCreditials.getPassword(),null);
            client.path(serviceUrl);

            StringBuilder requestUrlBuilder;
            requestUrlBuilder = new StringBuilder();
            requestUrlBuilder.append(servicePrefixUrl).append(serviceUrl);
            if (queryProperties != null) {
                Enumeration<?> enumeration;
                enumeration = queryProperties.propertyNames();
                while (enumeration.hasMoreElements()) {
                    String name = (String) enumeration.nextElement();
                    String value = StringUtils.trimToNull(queryProperties.getProperty(name));
                    if (value != null) {
                        client.query(name, value);
                    }
                }
            }
            client.query("inputFormat","json").query("outputFormat","json");
//            OAuthClientUtils.Consumer consumer = new OAuthClientUtils.Consumer(authCreditials.getConsumerKey(), authCreditials.getConsumerSecret());

//            String authorizationHeader = OAuthClientUtils.createAuthorizationHeader(
//                    consumer, new OAuthClientUtils.Token(authCreditials.getAccessToken(), authCreditials.getSecretToken()),
//                    requestMethod.name(), client.getCurrentURI().toString());

//            client.header("Authorization", authorizationHeader)
            client.header("Accept", "application/json").header("Content-type", "application/json");
            if (requestMethod.equals(ERequestMethod.GET)) {
                response = client.get(Response.class);
            } else if (requestMethod.equals(ERequestMethod.POST)) {
                response = client.post(data, Response.class);
            }
        } catch (Exception e) {
            throw new WfirmaApiException("Error running service [" + serviceUrl + "]", e);
        }
        returnString = response != null ? response.readEntity(String.class) : "{}";
        return returnString;
    }


    private String executeServiceGet(AuthCreditials authCreditials, String serviceUrl, Properties queryProperties) throws WfirmaApiException {
        return this.executeService(authCreditials, ERequestMethod.GET, serviceUrl, queryProperties, null, SERVICE_API_PREFIX_URL);
    }

    private String executeServicePost(AuthCreditials authCreditials, String serviceUrl, Properties queryProperties, String data) throws WfirmaApiException {
        return this.executeService(authCreditials, ERequestMethod.POST, serviceUrl, queryProperties, data, SERVICE_API_PREFIX_URL);
    }

    public AuthCreditials generateCreditialsWithToken(AuthCreditials authCreditials, String username, String password) throws WfirmaApiException {
        try {

            OAuthService service = new ServiceBuilder()
                    .provider(WfirmaAPI.class)
                    .apiKey(authCreditials.getConsumerKey())
                    .apiSecret(authCreditials.getConsumerSecret())
                    .scope("oauth-read")
                    .callback("https://wfirmacallback")
                    .build();
            Token requestToken = service.getRequestToken();
            String authUrl = service.getAuthorizationUrl(requestToken);

            AuthCreditials cloned = authCreditials.clone();
            String serviceUrl = "oauth/requestToken";
            ResponseDataProvider<Form> provider = new ResponseDataProvider<>();
            String baseAddress = BASE_ADDRESS;
            WebClient client = WebClient.create(baseAddress, Collections.singletonList(provider));
            client.path(serviceUrl);
//            client.header("Accept", "application/json").header("Content-type", "application/json");
            OAuthClientUtils.Consumer consumer = new OAuthClientUtils.Consumer(authCreditials.getConsumerKey(), authCreditials.getConsumerSecret());
            //client.query("x_auth_username", username).query("x_auth_password", password)
            client.query("oauth_callback", "https://hmail.pl/wfirmaoauth");//.query("x_auth_mode", "oauth_token");
            OAuthClientUtils.Token accessToken = OAuthClientUtils.getRequestToken(client, consumer, new URI(BASE_ADDRESS+serviceUrl), null);
            cloned.setAccessToken(accessToken.getToken());
            cloned.setSecretToken(accessToken.getSecret());
            return cloned;
        } catch (Exception e) {
            throw new WfirmaApiException("Error obtaining token from server", e);
        }
    }

    public String getUsers(AuthCreditials authCreditials) throws WfirmaApiException {
        String serviceUrl = "/users/get";
        return this.executeServiceGet(authCreditials, serviceUrl, null);
    }

    public String getSettingsUserData(AuthCreditials authCreditials) throws WfirmaApiException {
        String serviceUrl = "settings/user_data.json";

        return this.executeServiceGet(authCreditials, serviceUrl, null);
    }

    public String getClientsList(AuthCreditials authCreditials, Properties queryProperties) throws WfirmaApiException {
        String serviceUrl = "clients/list.json";

        return this.executeServiceGet(authCreditials, serviceUrl, queryProperties);
    }

    public String getClientsShow(AuthCreditials authCreditials, String id) throws WfirmaApiException {
        String serviceUrl = new StringBuilder().append("clients/show/").append(id).append(".json").toString();

        return this.executeServiceGet(authCreditials, serviceUrl, null);
    }

    public String postClientsUpdate(AuthCreditials authCreditials, String id, String data) throws WfirmaApiException {
        String serviceUrl = new StringBuilder().append("clients/update/").append(id).append(".json").toString();

        return this.executeServicePost(authCreditials, serviceUrl, null, data);
    }

    public String postClientsCreate(AuthCreditials authCreditials, String data) throws WfirmaApiException {
        String serviceUrl = new StringBuilder().append("clients/create").append(".json").toString();

        return this.executeServicePost(authCreditials, serviceUrl, null, data);
    }

    public String postClientsDelete(AuthCreditials authCreditials, String id) throws WfirmaApiException {
        String serviceUrl = new StringBuilder().append("clients/delete/").append(id).append(".json").toString();

        return this.executeServiceGet(authCreditials, serviceUrl, null);
    }

    public String getInvoicesList(AuthCreditials authCreditials, Properties queryProperties) throws WfirmaApiException {
        String serviceUrl = "invoices/list.json";

        return this.executeServiceGet(authCreditials, serviceUrl, queryProperties);
    }

    public String getInvoicesShow(AuthCreditials authCreditials, String id) throws WfirmaApiException {
        String serviceUrl = new StringBuilder().append("invoices/show/").append(id).append(".json").toString();

        return this.executeServiceGet(authCreditials, serviceUrl, null);
    }

    public String postInvoicesUpdate(AuthCreditials authCreditials, String id, String data) throws WfirmaApiException {
        String serviceUrl = new StringBuilder().append("invoices/update/").append(id).append(".json").toString();

        return this.executeServicePost(authCreditials, serviceUrl, null, data);
    }

    public String postInvoicesCreate(AuthCreditials authCreditials, String data) throws WfirmaApiException {
        String serviceUrl = new StringBuilder().append("invoices/create").append(".json").toString();

        return this.executeServicePost(authCreditials, serviceUrl, null, data);
    }

    public String postInvoicesDelete(AuthCreditials authCreditials, String id) throws WfirmaApiException {
        String serviceUrl = new StringBuilder().append("invoices/delete/").append(id).append(".json").toString();

        return this.executeServiceGet(authCreditials, serviceUrl, null);
    }

    public String postInvoicesPaid(AuthCreditials authCreditials, String id) throws WfirmaApiException {
        String serviceUrl = new StringBuilder().append("invoices/paid/").append(id).append(".json").toString();

        return this.executeServiceGet(authCreditials, serviceUrl, null);
    }

    public String postInvoicesPdf(AuthCreditials authCreditials, String id, EInvoiceDocType docType) throws WfirmaApiException {
        String serviceUrl = new StringBuilder().append("invoices/pdf/").append(id).append(".json").toString();
        Properties properties = new Properties();
        properties.setProperty("doc_type", docType.getCode());
        return this.executeServiceGet(authCreditials, serviceUrl, properties);
    }

}
