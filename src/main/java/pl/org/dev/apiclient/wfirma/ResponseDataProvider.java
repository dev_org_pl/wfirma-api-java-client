package pl.org.dev.apiclient.wfirma;
/*
 * Licensed to HMail.pl under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  HMail.pl licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.apache.cxf.common.util.StringUtils;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.ext.form.Form;
import org.apache.cxf.jaxrs.utils.HttpUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

/**
 * @author Cyprian Śniegota
 */
public class ResponseDataProvider<T>
        implements MessageBodyReader<T> {

    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mt) {
        return type.equals(Form.class);
    }

    public T readFrom(Class<T> type, Type genType, Annotation[] anns, MediaType mt,
                      MultivaluedMap<String, String> headers, InputStream is) throws IOException {
        String string = IOUtils.toString(is, HttpUtils.getEncoding(mt, "UTF-8"));
        Form form = null;
        try {
            form = (Form) type.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            return null;
        }

        List<String> parts = Arrays.asList(StringUtils.split(string, "&"));
        for (String part : parts) {
            String[] elements = StringUtils.split(part, "=");
            if (elements.length == 2) {
                form.set(elements[0], elements[1]);
            }

        }
        return (T) form;
    }

}