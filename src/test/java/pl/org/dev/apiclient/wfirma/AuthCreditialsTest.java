package pl.org.dev.apiclient.wfirma;

/*
 * Licensed to HMail.pl under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  HMail.pl licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Cyprian Śniegota
 */
public class AuthCreditialsTest {

    @Test
    public void toStringTest() {
        AuthCreditials authCreditials = new AuthCreditials("ckey", "csecret", null, null);
        String toString = authCreditials.toString();
        Assert.assertEquals("{'consumerKey':'ckey', 'consumerSecret':'csecret', 'accessToken:'null', 'secretToken:'null'}", toString);

        authCreditials = new AuthCreditials("ckey", "csecret", "atoken", "asecret");
        toString = authCreditials.toString();
        Assert.assertEquals("{'consumerKey':'ckey', 'consumerSecret':'csecret', 'accessToken:'atoken', 'secretToken:'asecret'}", toString);
    }

    @Test
    public void cloneTest() {
        AuthCreditials authCreditials = new AuthCreditials("ckey", "csecret", "atoken", "asecret");
        AuthCreditials cloned = authCreditials.clone();
        Assert.assertEquals(authCreditials, cloned);
    }
}