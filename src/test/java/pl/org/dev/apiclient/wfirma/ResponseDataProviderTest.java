package pl.org.dev.apiclient.wfirma;
/*
 * Licensed to HMail.pl under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  HMail.pl licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.apache.cxf.jaxrs.ext.form.Form;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Cyprian Śniegota
 */
public class ResponseDataProviderTest {

    @Test
    public void readFromTest() throws IOException {
        ResponseDataProvider<Form> responseDataProvider = new ResponseDataProvider<>();
        String str = "oauth_token=totototot&oauth_token_secret=tststststststs&usd=hashshsh";
        InputStream is = new ByteArrayInputStream(str.getBytes());
        Form form = responseDataProvider.readFrom(Form.class, null, null, null, null, is);
        Assert.assertEquals("totototot", form.getData().getFirst("oauth_token"));
        Assert.assertEquals("tststststststs", form.getData().getFirst("oauth_token_secret"));
    }

    public void isReadableTest() {
        ResponseDataProvider<Form> responseDataProvider = new ResponseDataProvider<>();
        boolean readable = responseDataProvider.isReadable(Form.class, null, null, null);
        Assert.assertEquals(true, readable);
    }
}
